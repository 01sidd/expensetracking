<!DOCTYPE html>
<html>
<head>
	<title>Expense Tracking Web App</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

	<!-- jQuery library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href="{{ asset('/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

</head>
<body>
	<div class="Container ">
		<div class="col-md-12 justify-content-center">
		<div class="col-md-10" style="margin-left: 80px;" >
			<h2 style="text-align: center;">Record Details</h2>
			<br/><br/>
			<div class="row">
				  <div class="form-group">
                       		<div class="col-md-6">
                            <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' class="form-control" name="date" placeholder="Select Date" autofocus required id="datesearch">
                                  <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                          </div>
                      </div>
				<button class="btn btn-success" id="searchBtn">Search</button>

				<button class="btn btn-primary" id="reset">Reset</button>

				</div>
			</div><br/>
			
			<div class="table-responsive">
	
			<table class="table table-bordered table-striped" style="text-align: center;" >
				<tr>
					<th style="text-align: center;">Date </th>
					<th style="text-align: center;">Categories </th>
					<th style="text-align: center;">Type </th>
					<th style="text-align: center;">Amount</th>
					<th style="text-align: center;">Action</th>
				</tr>
				<tbody id="tBody">
					
				</tbody>
					
			</table>
			</div>
		</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			url="/recordSearch";
			search(url);
		})

		$("#reset").click(function(){
			 $("#datesearch").val("");
			 var url="/recordSearch";
			 search(url);			 
			})

		$("#searchBtn").click(function(){
			 var query=$("#datesearch").val();
			 var url="/recordSearch/"+query;
			 search(url);			 
			})

		function search(url){
			$.ajax({
			   	url:url,
			   	method:'GET',
			   	dataType:'json',
			   	success:function(data)
			   	{
                    $('#tBody').html("");
			   		var trHTML = '';
                        $.each(data.list, function (i, resp) {
                                trHTML +=
                                    '<tr><td>'
                                    + resp.date
                                    + '</td><td>'
                                    + resp.categories
                                    + '</td><td>'
                                    + resp.type
                                    + '</td><td>'
                                    + resp.amount 
                                    + '</td><td>'
                                    + '<a href="/expense/'+resp.id+'/edit" class="btn btn-primary">Edit</a>'
                                    + '<a href="/expense/delete/'+resp.id+' " class="btn btn-danger">Delete</a>'
                                    + '</td></tr>';
                        });
                        $('#tBody').append(trHTML);
		  		}
		  	})}
	</script>
	<script src="{{ asset('js/moment.js') }}"></script>
  	  <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
	<script>
	$(document).ready(function () {
  	var m = moment(new Date());
  	$('#datetimepicker1').datetimepicker({
  		format: 'DD-MM-YYYY',
	 });
  });
</script>
</body>
</html>
