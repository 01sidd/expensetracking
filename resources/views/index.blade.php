<!DOCTYPE html>
<html>
<head>
	<title>Expense Tracking Web App</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

	<!-- jQuery library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="{{ asset('/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
</head>
<body>
	<div class="container" >
		<div class="row col-md-6" style="left: 200px; margin: 25px 50px 75px 100px; border-spacing: 5px; border: 1px solid black;">
		<form id="form" action="{{route('expense.store')}}" method="post" >
			@csrf
			<h2 style="text-align: center;">Expense Track App</h2>

			<div class="form-group">
                       	<label>Date:</label>
                            <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' class="form-control" name="date" placeholder="Select Date" autofocus required>
                                  <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                          </div>
			</div><br/>
			<div class="form-group">
				<label>Categories : </label>
				<select name="categories" class="form-control" required>
					<option value="">  </option>
					<option value="SAVINGS"> SAVINGS </option>
					<option value="CURRENT"> CURRENT </option>
					<option value="JOINT"> JOINT </option>
				</select>
			</div><br/>
			<div class="form-group">
				<label>Type : </label>
				<input type="radio" name="type" value="cash"> Cash &nbsp
				<input type="radio" name="type" value="credit"> Credit
			</div><br/>
			<div class="form-group">
				<label>Amount : </label>
				<input type="text" name="amount" class="form-control" required>
			</div>
			<div class="form-group" >
				<input type="submit"  class="btn btn-primary" > 
				<a href="/expense/show" target="_blank" class="btn btn-primary" style="float: right;"> Details </a>  
			</div>
		</form>
	</div>
	</div>
	  <script src="{{ asset('js/moment.js') }}"></script>
  	  <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-
    methods.min.js"></script>
	<script>
	    $(document).ready(function () {
		    $('#form').validate({ // initialize the plugin
		        rules: {
		            date: {
		                required: true
		            },
		            categories: {
		                required: true
		            },
		            type: {
		                required: true
		            },
		            amount: {
		                required: true,
		                number: true
		            },
		        }
	    	});
		});
	</script>
<script>
$(document).ready(function () {
  var m = moment(new Date());
  $('#datetimepicker1').datetimepicker({
  		format: 'DD-MM-YYYY',
	 });
  });
</script>
</body>
</html>