<!DOCTYPE html>
<html>
<head>
	<title> Expense Tracking Web App </title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

	<!-- jQuery library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="{{ asset('/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
</head>
<body>
	<div class="container" >
		<div class=" row col-md-8">
		
		<form action="/edit/{{ $data->id }}" method="POST" >
			@csrf
			<h2>Edit Expense</h2>
			<div class="form-group">

				<label>Date:</label>
                            <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' class="form-control" name="date" 
                                  value="{{$data->date}}" placeholder="Select Date" autofocus required>
                                  <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                          </div>
			</div><br/>
			<div class="form-group">
				<label>Categories :</label>
				<input type="text" name="categories" class="form-control" value="{{$data->categories}}" required autofocus>
			</div><br/>
			<div class="form-group">
				<label>Type :</label>
				<input type="text" name="type" class="form-control" value="{{$data->type}}" required autofocus>
			</div><br/>
			<div class="form-group">
				<label>Amount :</label>
				<input type="text" name="amount" class="form-control" value="{{$data->amount}}" required autofocus>
			</div><br/>
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
	  <script src="{{ asset('js/moment.js') }}"></script>
  	  <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
	<script>
		$(document).ready(function () {
		  var m = moment(new Date());
		  $('#datetimepicker1').datetimepicker({
		  		format: 'DD-MM-YYYY',
			 });
		  });
	</script>
</body>
</html>