<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Expense;
use Validator;



class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation rules Starts here -->
        $rules =array(
            'date'=>'required',
            'categories'=>'required',
            'type' => 'required',
            'amount' => 'required',
        );


        $validator = Validator::make($request->all(), $rules);
            //Check to see if validation fails or passes
        if($validator->fails())
        {
            // Alert::error('Error','Somthing went wrong.. Please check errors..! ');
            return redirect()->back()->withErrors($validator)->withInput();
        }  

        $crud = new Expense();
        $crud->date = $request->date;
        $crud->categories = $request->categories;
        $crud->type = $request->type;
        $crud->amount = $request->amount;

        $crud->save();
        return redirect('/expense')->with('success','User with Image has been added..!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // $all = Expense::paginate(5);
        return view('details');
    }
    public function expenseJson(Request $request,$query=null)
    {   
        if(!empty($query)){
        $all = Expense::where('date','like','%'.$query.'%')->get();            
        }else{
        $all = Expense::all();          
        }
        return response()->json([
           'status'=>'success',
           'message'=>'all records',
           'list'=>$all,
         ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Expense::find($id);
        return view('edit',compact('data','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Expense::findOrFail($id);
        $data->date = $request->date;
        $data->categories = $request->categories;
        $data->type = $request->type;
        $data->amount = $request->amount;

        $data->save();
        return redirect('/expense/show');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = Expense::findOrFail($id);
        $crud->delete();
        return redirect()->back();
    }

    
}
